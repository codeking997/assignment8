﻿using System;
using System.Collections.Generic;

namespace assignment5
{
    class Program
    {
        static BorderCollie borderCollie = new BorderCollie();

        static Puddle puddle = new Puddle();

        static Lion lion = new Lion();

        static Tiger tiger = new Tiger();

        /*
         *I put the hamster and the dog and the cat in their own methods to make it more neat
         *the plan was to do on the last assignment but I forgot so here it is 
         * 
         */
        static void Main(string[] args)
        {


            HamsterMethod();

            DogMethod();

            CatMethod();

            
        }
        private static  void CatMethod()
        {
            List<Cat> catList = new List<Cat>();
            catList.Add(new Tiger() { Name = "Mittens", age = 2 });
            catList.Add(new Lion() { Name = "Findus", age = 4 });
            foreach (Cat aCat in catList)
            {
                Console.WriteLine($"{aCat.Name}, {aCat.age}");
                aCat.Eat();
                aCat.MakeNoise();
                aCat.ChaseMice();
                if (aCat.catRace == "Lion")
                {
                    lion.HatesWater();
                }
                else if (aCat.catRace == "Tiger")
                {
                    tiger.TakesBath();
                }
            }
        }
        private static void HamsterMethod()
        {
            List<Hamster> hamList = new List<Hamster>();
            hamList.Add(new Hamster() { Name = "Hamtaro", age = 2 });
            hamList.Add(new Hamster() { Name = "Kiko", age = 3 });
            foreach (Hamster aHamster in hamList)
            {
                Console.WriteLine($"{aHamster.Name}, {aHamster.age}");
                aHamster.Eat();
                aHamster.MakeNoise();
                aHamster.RunInWheel();
            }

        }
        private static  void DogMethod()
        {
            List<Dog> dogList = new List<Dog>();
            dogList.Add(new Puddle() { Name = "Fido", age = 5 });
            dogList.Add(new BorderCollie() { Name = "Rex", age = 14 });

            foreach (Dog aDog in dogList)
            {
                Console.WriteLine($"{aDog.Name}, {aDog.age}");
                aDog.Eat();
                aDog.MakeNoise();
                aDog.MarkTerritory();
                if (aDog.dogRace == "BorderCollie")
                {
                    borderCollie.ChaseSheep();
                    borderCollie.OpenDoor();
                    borderCollie.SpeaksPortugeese();
                }
                else if (aDog.dogRace == "Puddle")
                {
                    puddle.PlayDressUp();
                    puddle.SitInHandBag();
                }

            }
        }
    }
}
