﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment5
{
    public class Animal : IMovement
    {
        
        public string Name { get; set; }

        public int age { get; set; }

        public void Eat()
        {
            Console.WriteLine("namnamnam");
        }
        public virtual void MakeNoise()
        {
            Console.WriteLine("muuu");
        }
        //I made a run method in the run interface and implemented it here since all animals run
        public  void Run()
        {
            Console.WriteLine("RUN SUPER FAST ANIMALS");
        }
    }
}
